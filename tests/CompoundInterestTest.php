<?php
class CompoundInterestTest extends PHPUnit\Framework\TestCase
{
  public function testCompoundCalculate()
  {
    $compoundInterest = new \Denis\CompoundInterest(50000, 10, 3);
    $result = $compoundInterest->calculate();
    $this->assertEquals(66550, $result);
  }
}
