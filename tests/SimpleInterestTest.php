<?php
class SimpleInterestTest extends PHPUnit\Framework\TestCase
{
  public function testSimpleCalculate()
  {
    $simpleInterest = new \Denis\SimpleInterest(50000, 10, 3);
    $result = $simpleInterest->calculate();
    $this->assertEquals(65000, $result);
  }
}
