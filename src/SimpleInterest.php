<?php

namespace Denis;

class SimpleInterest
{
  private $debtAmount = 0;
  private $interestRate = 0;
  private $chargesNumber = 0;

  function __construct($d, $i, $c)
  {
    $this->debtAmount = $d;
    $this->interestRate = $i;
    $this->chargesNumber = $c;
  }

  function calculate()
  {
    return round($this->debtAmount * (1 + ($this->interestRate / 100) *  $this->chargesNumber));
  }
}
